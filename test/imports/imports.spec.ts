import { expect } from "chai";

import * as imports from "../../src/index";

describe("export checks", () => {
  describe("request", () => {

    it("correctly exports fetchRequestPayload", () => {
      expect(imports.fetchRequestPayload).to.be.a("function");
    });

    it("correctly exports createQrForRequest", () => {
      expect(imports.createQrForRequest).to.be.a("function");
    });

    it("Correctly exports createJWT", () => {
      expect(imports.createJWT).to.be.a("function");
    });

    it("Correctly exports openJWT", () => {
      expect(imports.openJWT).to.be.a("function");
    });

    it("Correctly exports ClaimSetKeys", () => {
      expect(imports.ClaimSetKeys).to.be.a("function");
    });

    it("Correctly exports OpenClaimSet", () => {
      expect(imports.openClaimSet).to.be.a("function");
    });

    it("Correctly exports Attestation", () => {
      expect(imports.Attestation).to.be.a("function");
    });

    it("Correctly exports openAttestation", () => {
      expect(imports.openAttestation).to.be.a("function");
    });

  });
});
