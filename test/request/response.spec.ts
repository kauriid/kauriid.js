import { expect } from "chai";
import jsdom from "jsdom-global";
import webcrypto from "node-webcrypto-shim";
import sinon, { SinonFakeTimers, SinonSpy } from "sinon";

import { Attestation } from "../../src/claims/attestation";
import { buildResponse } from "../../src/request/response";

const SANDBOX = sinon.createSandbox();

describe("Response", () => {
  // dirty hack to inject WebCrypto API for Node
  (global as any).crypto = webcrypto;

  after(() => {
    SANDBOX.reset();
    delete (global as any).crypto;
  });

  it("Can create a response object", async () => {
    const fakeAttestation1 = SANDBOX.createStubInstance(Attestation);
    fakeAttestation1.finalise.resolves("fakeAttestation1");
    const fakeAttestation2 = SANDBOX.createStubInstance(Attestation);
    fakeAttestation2.finalise.resolves("fakeAttestation2");

    const fakeKey1: CryptoKey = {
      algorithm: { name: "AES-GCM" },
      extractable: true,
      type: "secret",
      usages: ["decrypt", "encrypt"]
    };

    const fakeKey2: CryptoKey = {
      algorithm: { name: "C20P" },
      extractable: true,
      type: "secret",
      usages: ["decrypt", "encrypt"]
    };

    const fakeClaimKeys1 = {
      1: fakeKey1,
      3: fakeKey1
    };

    const fakeClaimKeys2 = {
      2: fakeKey1,
      6: fakeKey1
    };

    const exportKeyStub = SANDBOX.stub(
      (global as any).crypto.subtle,
      "exportKey"
    ).onFirstCall().resolves({ k: "FakeKey1" });
    exportKeyStub.onSecondCall().resolves({ k: "FakeKey2" });

    const response = await buildResponse(
      "Issuer",
      "****************." +
      "****************************." +
      "******************************",
      [
        fakeAttestation1 as unknown as Attestation,
        fakeAttestation2 as unknown as Attestation
      ],
      [fakeKey1, fakeKey2],
      [fakeClaimKeys1, fakeClaimKeys2]
    );

    expect(response.vc.length).to.equal(2);
    expect(response.vc[0].objectKey).to.deep.equal({
      k: "FakeKey1"
    });
    expect(response.vc[1].objectKey).to.deep.equal({
      k: "FakeKey2"
    });
    expect(response.iss).to.equal("Issuer");
    expect(response.vc[0].claimKeys).to.deep.equal(fakeClaimKeys1);
    expect(response.vc[1].claimKeys).to.deep.equal(fakeClaimKeys2);
    expect(response.vc[0].attestation).to.equal("fakeAttestation1");
    expect(response.vc[1].attestation).to.equal("fakeAttestation2");
  });

});
