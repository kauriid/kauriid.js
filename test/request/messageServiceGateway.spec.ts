import { expect } from "chai";
import jsdom from "jsdom-global";
import "mocha";
import * as sinon from "sinon";

import "mocha";

import { pollForResponse } from "../../src/request/messageServiceGateway";
const sandbox = sinon.createSandbox();


let jsdomCleanup: () => void;

const errorResponse = {
  json: () => {
    return {
      message: {}
    };
  },
  status: 500,
  statusText: "Internal Server Error"
};

const emptyResponse = {
  json: () => {
    return {
      message: {}
    };
  },
  status: 200,
  statusText: "OK"
};

const realResponse = {
  json: () => {
    return {
      message: { payload: "Not Empty!" }
    };
  },
  status: 200,
  statusText: "OK"
};

describe("Message Service Gateway", () => {

  before(() => {
    sandbox.stub(global, "setTimeout").callsFake(
      (callback: any, time: any) => {
        callback();
        return {
          ref: sinon.fake(),
          refresh: sinon.fake(),
          unref: sinon.fake(),
        };
      });
  });

  beforeEach(() => {
    jsdomCleanup = jsdom({}, {});
    window.fetch = () => Promise.reject("Called Fake Fetch");
  });

  afterEach(() => {
    jsdomCleanup();
  });

  after(() => {
    jsdomCleanup();
    sandbox.reset();
  });

  describe("pollForResponse", () => {
    it("will retry for a non-empty response", async () => {
      sandbox
        .stub(window, "fetch")
        .onFirstCall()
        .resolves(emptyResponse)
        .onSecondCall()
        .resolves(realResponse);

      const pollPromise = pollForResponse("fake.notreal.co.nz/imaginary");
      const message = await pollPromise;
      expect(message).to.deep.equal({ payload: "Not Empty!" });
    });

    it("will throw an error after 5 consecutive error responses.",
      async () => {
        sandbox.stub(window, "fetch").resolves(errorResponse);
        try {
          const pollPromise = await pollForResponse(
            "fake.notreal.co.nz/imaginary"
          );
          expect(false).to.equal(true);
        } catch (error) {
          expect(error.message).to.equal(
            "Failed to contact message service five times"
          );
        }
      });
      
  });
});
