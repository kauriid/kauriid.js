import { expect } from "chai";
import jsdom from "jsdom-global";

import { 
  createQrForRequest,
  fetchRequestPayload 
} from "../../src/request/request";

describe("Requests", () => {

  let jsdomCleanup: () => void;

  beforeEach(() => {
    jsdomCleanup = jsdom({}, {});
  });

  afterEach(() => {
    jsdomCleanup();
  });

  describe("QR Generation", () => {

    it("Generates a QR code for a payload", () => {
      const canvas = window.document.createElement("canvas");
      const payload = "I see a little silhouetto of a man, Scaramouche, " +
        "Scaramouche, will you do the Fandango\? Thunderbolt and lightning, " +
        "Very, very frightening me. (Galileo) Galileo. (Galileo) " +
        "Galileo, Galileo Figaro Magnifico-o-o-o-o.";
      window.document.body.appendChild(canvas);

      createQrForRequest(payload, canvas, 500);
      const dataurl = (canvas as any).qrious.toDataURL();
      expect(dataurl).to.be.a("string");
    });

    it("will default size to 500px", () => {
      const payload =
        "This qr code should be 500px, because we are defaulting the " +
        "parameter to 500.";
      const canvas = window.document.createElement("canvas");
      window.document.body.appendChild(canvas);
      createQrForRequest(payload, canvas);

      expect(canvas.width).to.equal(500);
    });
  });

  describe("Fetch Request Payload", () => {
    it("Placeholder", () => {
      const result = fetchRequestPayload({});
      expect(result).to.be.a("string");
      expect(result).to.equal("");
    });
  });

});
