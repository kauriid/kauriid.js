import { expect } from "chai";
import "mocha";

import { openAttestation } from "../../src/claims/attestation";

import referenceData from "../resources/reference_data.json";
import {
  A256GCMsymericKey,
  badHeaderAttestation,
  Ed25519privSignJwk
} from "../resources/testData";

describe("Attestation", () => {
  it("can open a serialized attestation", async () => {
    const testAttestation = referenceData
      .cases[0].steps[1].output.attestation!;
    const objectKeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.object_key;
    const objectKey = await crypto.subtle.importKey(
      "jwk",
      objectKeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    const signersKey = referenceData.cases[0].data.jwks.dumbledore_sig_pub;
    const openedAttestation = await openAttestation(
      testAttestation,
      objectKey,
      signersKey
    );

    const claim0KeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.claim_keys[0];
    const claim0Key = await crypto.subtle.importKey(
      "jwk",
      claim0KeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );

    const claim = await openedAttestation.accessClaim(0, claim0Key);
    expect(claim).to.deep.equal(referenceData.cases[0].data.harry_claims[4]);
  });

  it("errors when claim Hash does not match stored hash", async () => {
    const testAttestation = referenceData
      .cases[0].steps[1].output.attestation!;
    const objectKeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.object_key;
    const objectKey = await crypto.subtle.importKey(
      "jwk",
      objectKeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    const signersKey = referenceData.cases[0].data.jwks.dumbledore_sig_pub;
    const openedAttestation = await openAttestation(
      testAttestation,
      objectKey,
      signersKey
    );

    const claim0KeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.claim_keys[0];
    const claim0Key = await crypto.subtle.importKey(
      "jwk",
      claim0KeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    // Corrupt the claim's salt so it won't match
    // tslint:disable-next-line:no-string-literal
    openedAttestation["attesterCommitmentSalts"][0] = Buffer.from(
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    );
    const claim = openedAttestation.accessClaim(0, claim0Key);
    expect(claim).to.eventually.be.rejectedWith(Error);
  });

  it("can cosign a foreign attestation", async () => {
    const testAttestation = referenceData
      .cases[0].steps[1].output.attestation!;
    const objectKeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.object_key;
    const objectKey = await crypto.subtle.importKey(
      "jwk",
      objectKeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    const signersKey = referenceData.cases[0].data.jwks.dumbledore_sig_pub;
    const openedAttestation = await openAttestation(
      testAttestation,
      objectKey,
      signersKey
    );

    const harrySigningKey = referenceData.cases[0].data.jwks.harry_sig_full;

    openedAttestation.cosign(harrySigningKey);

    // Do we have two commitments?
    expect(Object.values(openedAttestation.commitments).length).to.equal(2);
    // Is the subject commitment the shape we expect?
    expect(openedAttestation.commitments.subject).to.be.a("string");
    expect(openedAttestation.commitments.subject.split(".")).length(3);
    // Is the attester commitment the shape we expect?
    expect(openedAttestation.commitments.attester).to.be.a("string");
    expect(openedAttestation.commitments.attester.split(".")).length(3);
  });

  it("can finalize an attestation", async () => {
    const testAttestation = referenceData
      .cases[0].steps[1].output.attestation!;
    const objectKeyJWK = referenceData.cases[0].steps[1].output
      .claim_set_keys.object_key;
    const objectKey = await crypto.subtle.importKey(
      "jwk",
      objectKeyJWK,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    const signersKey = referenceData.cases[0].data.jwks.dumbledore_sig_pub;

    const openedAttestation = await openAttestation(
      testAttestation,
      objectKey,
      signersKey
    );

    const finalised = await openedAttestation.finalise(objectKey);
    expect(finalised.split(".").length).to.equal(5);
  });

  it("Will error when opening an Attestation with bad headers", async () => {
    const signingKey = Ed25519privSignJwk;
    const objectKey = await crypto.subtle.importKey(
      "jwk",
      A256GCMsymericKey,
      { name: "AES-GCM", length: 256 },
      true,
      ["encrypt", "decrypt"]
    );
    const openedAttestation = openAttestation(
      badHeaderAttestation, objectKey, signingKey
    );
    expect(openedAttestation).to.eventually.be.rejectedWith(Error);
  });

});
