import * as chai from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import webcrypto from "node-webcrypto-shim";

import { ClaimSetKeys, openClaimSet } from "../../src/claims/claimSetKeys";
import {
  A256GCMsymericKey,
  addressClaim,
  badClaimSet,
  Ed25519privSignJwk,
  Ed25519pubSignJwk,
  goodClaimSet,
  phoneNumberClaim,
  potterClaim,
} from "../resources/testData";

chai.use(chaiAsPromised);
const expect = chai.expect;

describe("ClaimSetKeys", () => {
  // dirty hack to inject WebCrypto API for Node
  (global as any).crypto = webcrypto;

  const phoneNumber = {
    "@context": "http://schema.org",
    "@type:": "Person",
    telephone: "0800424242"
  };

  const address = {
    "@context": "http://schema.org",
    "@type": "Person",
    address: {
      "@type": "PostalAddress",
      streetAddress: "4 Privet Drive"
    }
  };

  const jwk = {
    alg: "A256GCM",
    ext: true,
    k: "4DV36GqbzLaJ4kaP50zmcyy9NjBPOhhySejSJjhsKII",
    key_ops: ["encrypt, decrypt"],
    kty: "oct",
  };

  it("Can add a claim", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    const promise = claimSetKeys.addClaim(potterClaim);
    await promise;
  });

  it("Can access a claim", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    await claimSetKeys.addClaim(potterClaim);

    const key = claimSetKeys.claimKeys[0];
    const claim = await claimSetKeys.accessClaim(0, key);

    expect(claim).to.deep.equal(potterClaim);
  });

  it("Can access a claim as bytes", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    await claimSetKeys.addClaim(potterClaim);

    const key = claimSetKeys.claimKeys[0];
    const claim = await claimSetKeys.accessClaim(0, key, true);

    expect(claim).to.deep.equal(Buffer.from(JSON.stringify(potterClaim)));
  });

  it("can access all claims in a claimset", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    const promises: Array<Promise<void>> = [];
    promises.push(claimSetKeys.addClaim(potterClaim));
    promises.push(claimSetKeys.addClaim(phoneNumberClaim));
    promises.push(claimSetKeys.addClaim(addressClaim));
    await Promise.all(promises);

    const keys = Object.values(claimSetKeys.claimKeys);
    const claims = await claimSetKeys.accessAllClaims(keys);
    expect(claims).to.deep.include(potterClaim);
    expect(claims).to.deep.include(phoneNumberClaim);
    expect(claims).to.deep.include(addressClaim);
  });

  it("will error when trying to access all claims in a claimset, without all the keys", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    const promises: Array<Promise<void>> = [];
    promises.push(claimSetKeys.addClaim(potterClaim));
    promises.push(claimSetKeys.addClaim(phoneNumberClaim));
    promises.push(claimSetKeys.addClaim(addressClaim));
    await Promise.all(promises);

    const keys = Object.values(claimSetKeys.claimKeys);
    const keysSlice = keys.slice(2);
    const claimsPromise = claimSetKeys.accessAllClaims(keysSlice);
    expect(claimsPromise).to.eventually.be.rejectedWith(Error);
  });

  it("will error when index is out of bounds", async () => {
    const claimSetKeys = new ClaimSetKeys("Harry Potter");
    await claimSetKeys.addClaim(potterClaim);
    const key = claimSetKeys.claimKeys[0];
    const claim = claimSetKeys.accessClaim(42, key);
    expect(claim).to.eventually.be.rejectedWith(Error);
  });

  describe("Finalise", () => {
    it("Can finalise a claimSet", async () => {
      const claimSetKeys = new ClaimSetKeys("Harry Potter");
      await claimSetKeys.addClaim(potterClaim);
      await claimSetKeys.addClaim(phoneNumberClaim);
      await claimSetKeys.addClaim(addressClaim);
      claimSetKeys.signingKey = Ed25519privSignJwk;
      claimSetKeys.objectKey = await (global as any).crypto.subtle.importKey(
        "jwk",
        A256GCMsymericKey,
        {
          iv: new Uint8Array([25, 181, 88, 0, 219, 12, 250, 72, 251, 23, 228,28]),
          name: "AES-GCM",
        },
        true,
        ["encrypt", "decrypt"]
      );
      const finalisedClaimSet = await claimSetKeys.finaliseClaimSet(true);
    });

    it("Can finalise a claimSet with shuuffled claims", async () => {
      const claimSetKeys = new ClaimSetKeys("Harry Potter");
      await claimSetKeys.addClaim(potterClaim);
      await claimSetKeys.addClaim(phoneNumberClaim);
      await claimSetKeys.addClaim(addressClaim);
      claimSetKeys.signingKey = Ed25519privSignJwk;
      claimSetKeys.objectKey = await (global as any).crypto.subtle.importKey(
        "jwk",
        A256GCMsymericKey,
        {
          iv: new Uint8Array([25, 181, 88, 0, 219, 12, 250, 72, 251, 23, 228,28]),
          name: "AES-GCM",
        },
        true,
        ["encrypt", "decrypt"]
      );

      const finalisedClaimSet = await claimSetKeys.finaliseClaimSet(false);
    });

    it("Finalise errors without an object key", async () => {
      const claimSetKeys = new ClaimSetKeys("Harry Potter");
      await claimSetKeys.addClaim(potterClaim);
      claimSetKeys.signingKey = Ed25519privSignJwk;

      expect(claimSetKeys.finaliseClaimSet(true)).to.be.rejectedWith(Error);
    });

    it("Finalise errors without an signing key", async () => {
      const claimSetKeys = new ClaimSetKeys("Harry Potter");
      await claimSetKeys.addClaim(potterClaim);
      claimSetKeys.objectKey = await (global as any).crypto.subtle.importKey(
        "jwk",
        A256GCMsymericKey,
        {
          iv: new Uint8Array([25, 181, 88, 0, 219, 12, 250, 72, 251, 23, 228,28]),
          name: "AES-GCM",
        },
        true,
        ["encrypt", "decrypt"]
      );

      expect(claimSetKeys.finaliseClaimSet(true)).to.be.rejectedWith(Error);
    });
  });

  describe("Open", () => {

    it("can open a good ClaimSet", async () => {
      const objectKey = await (global as any).crypto.subtle.importKey(
        "jwk",
        A256GCMsymericKey,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
      );
      const signingKey = Ed25519pubSignJwk;
      const claimSetKeys = await openClaimSet(goodClaimSet, objectKey, signingKey);
    });

    it("open will error if commitment cannot be verified", async () => {
      const objectKey = await (global as any).crypto.subtle.importKey(
        "jwk",
        A256GCMsymericKey,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
      );
      const signingKey = Ed25519pubSignJwk;
      const claimSetKeys = openClaimSet(badClaimSet, objectKey, signingKey);

      expect(claimSetKeys).to.be.rejectedWith(Error);
    });

  });
});
