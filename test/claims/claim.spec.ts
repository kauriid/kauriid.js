import { expect } from "chai";
import "mocha";
import webcrypto from "node-webcrypto-shim";

import { decryptClaim, encryptClaim } from "../../src/claims/claim";

describe("Claims", () => {
  // dirty hack to inject WebCrypto API for Node
  (global as any).crypto = webcrypto;

  const potterClaim = {
    '@context': 'http://schema.org',
    '@type': 'Person',
    'familyName': 'Potter'
  };

  const mercuryClaim = {
    '@context': 'http://schema.org',
    '@type': 'Person',
    'familyName': 'Mercury',
    'givenName': 'Freddie',
  };

  const encryptedMercuryClaim = "eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0.." +
    "3gQ4GZUCdW3UeiBs.e75z_qXZcYT9nw8BF8cQlaBO_9c9tUzO4OlrB-FiED7N6_nkeIyzL7" +
    "I-fAIcKCPxYxIU7NgCBtce_m2dUx0fjI-sv8jIhrsESIBR-MAaLkZirAfDRrhu_Q7WBNwlN" +
    "Q.qFIddoyxrF_FLDPIoQWKIQ";
  const mercuryKey = "4DV36GqbzLaJ4kaP50zmcyy9NjBPOhhySejSJjhsKII";

  it("can encrypt a claim with A256GCM", async () => {
    const encryptedClaim = await encryptClaim(potterClaim);
    expect(encryptedClaim.ciphertextClaim).to.be.a("string");
  });

  it("can decrypt a claim with A256GCM", async () => {
    const jwk = {
      alg: "A256GCM",
      ext: true,
      k: mercuryKey,
      key_ops: ["encrypt, decrypt"],
      kty: "oct",
    };
    // Acessing "crypto" module that is not on the default NodeJs global type.
    const key =
      (global as any).crypto.subtle.importKey(
        "jwk",
        jwk,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
      );

    const decryptedClaim = await decryptClaim(encryptedMercuryClaim, key);
    expect(decryptedClaim).to.deep.equal(mercuryClaim);
  });

  it("can perform a round trip (encrypt and decrypt) with A256GCM",
    async () => {
      const encryptedClaim = await encryptClaim(potterClaim);
      expect(encryptedClaim.ciphertextClaim).to.be.a("string");

      const decryptedClaim = await decryptClaim(
        encryptedClaim.ciphertextClaim,
        encryptedClaim.key
      );
      expect(decryptedClaim).to.deep.equal(potterClaim);
    });
});
