import { expect } from "chai";
import "mocha";
import webcrypto from "node-webcrypto-shim";

import { getRandomOrder } from "../../src/crypto/utils";

describe("GetRandomOrder", () => {

  it("returns a random order", () => {
    // dirty hack to inject WebCrypto API for Node
    (global as any).crypto = webcrypto;

    const order = getRandomOrder(100);
    const sortedOrder = order.slice().sort();
    expect(order).to.not.equal(sortedOrder);
    expect(order.length).to.equal(sortedOrder.length);
    expect(order.values).to.equal(sortedOrder.values);
  });
});
