import { expect } from "chai";
import "mocha";
import * as sinon from "sinon";

import { getCrypto, setCryptoObject } from "../../src/crypto/utils";

const sandbox = sinon.createSandbox();

const fakeCrypto = { getRandomValues: sinon.spy(), subtle: undefined };

describe("getCrypto", () => {

  beforeEach(() => {
    if (!(global as any).window) {
      Object.assign(global, { window: {} });
    }
    sandbox.stub(global, "window" as any).value(
      {
        crypto: fakeCrypto
      });
  });

  afterEach(() => {
    sandbox.restore();
    delete (global as any).window;
  });

  it("returns window.crypto when present", () => {
    const gotCrypto = getCrypto();
    expect(gotCrypto).to.deep.equal(fakeCrypto);
  });

  it("returns global.crypto when window.crypto is not present", () => {

    delete (global as any).window;
    (global as any).crypto = fakeCrypto;
    expect(getCrypto()).to.equal(fakeCrypto);

    delete (global as any).crypto;
  });

  it("reassigns window.crypto.subtle to window.webkitcrypto.subtle" +
    "when it exists",
    () => {
      sandbox.replace(
        window,
        "crypto" as any,
        { webkitSubtle: { foo: "bar" } }
      );

      expect(getCrypto().subtle).to.equal((window.crypto as any).webkitSubtle);

    });

  it("returns the cached Crypto object," +
    "when the user has called setCryptoObject",
    () => {
      setCryptoObject(fakeCrypto);
      expect(getCrypto()).to.equal(fakeCrypto);

      setCryptoObject(undefined as any);
    });
});
