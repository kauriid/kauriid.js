import { expect } from "chai";

import { getPublicKeyFromPrivateKey, IPrivateEcKey} from "../../src/crypto/keys";
import { ed25519Signer, secp256k1Signer } from "../../src/crypto/signer";
import { Ed25519privSignJwk, ES256KprivSignJwk, expectedMessage, message } from '../resources/testData';

describe("Signer", () => {
  describe("secp256k1", () => {
    it("Can sign a payload with secp256k1", () => {
      const sig = secp256k1Signer.sign({ d: ES256KprivSignJwk.d }, message);
      expect(message).to.equal(expectedMessage);
      expect(sig).to.be.a("string");
    });

    it("Throws when given empty string Key", () => {
      const shouldThrow = () => {
        secp256k1Signer.sign({ d: "" }, message);
      };
      expect(shouldThrow).to.throw();
    });

    it("Throws when given undefined Key", () => {
      const shouldThrow = () => {
        // "as unknown as string" to force Typescript into letting us pass 
        // undefiend, when the type system knows we shouldn't
        // don't do this in real code please.
        secp256k1Signer.sign((undefined as unknown) as IPrivateEcKey, message);
      };
      expect(shouldThrow).to.throw();
    });

    it("Throws when given a Key with d undefiend", () => {
      const shouldThrow = () => {
        // "as unknown as string" to force Typescript into letting us pass 
        // undefiend, when the type system knows we shouldn't
        // don't do this in real code please.
        secp256k1Signer.sign({ d: (undefined as unknown) as string }, message);
      };
      expect(shouldThrow).to.throw();
    });
  });

  describe("Ed25519", () => {
    it("Can sign a payload with Ed25519", () => {
      const sig = ed25519Signer.sign({ d: Ed25519privSignJwk.d }, message);
      expect(message).to.equal(expectedMessage);
      expect(sig).to.be.a("string");
    });

    it("Throws when given empty string Key", () => {
      const shouldThrow = () => {
        ed25519Signer.sign({ d: "" }, message);
      };
      expect(shouldThrow).to.throw();
    });

    it("Throws when given undefined Key", () => {
      const shouldThrow = () => {
        // "as unknown as IPrivateEcKey" is to force
        // Typescript into letting us pass undefiend
        // when the type system knows we shouldn't
        // don't do this in real code please.
        ed25519Signer.sign((undefined as unknown) as IPrivateEcKey, message);
      };
      expect(shouldThrow).to.throw();
    });

    it("Throws when given a Key with d undefiend", () => {
      const shouldThrow = () => {
        // "as unknown as string" to force Typescript into letting us pass 
        // undefiend, when the type system knows we shouldn't
        // don't do this in real code please.
        ed25519Signer.sign({ d: (undefined as unknown) as string }, message);
      };
      expect(shouldThrow).to.throw();
    });
  });

  it("Can get PublicKey from secp256k1 PrivateKey", () => {
    const publicKey = getPublicKeyFromPrivateKey(
      ES256KprivSignJwk,
      "secp256k1"
    );

    expect(publicKey.x).to.equal(ES256KprivSignJwk.x);
    expect(publicKey.y).to.equal(ES256KprivSignJwk.y);
  });

  it("Can get PublicKey from Ed25519 PrivateKey", () => {
    const publicKey = getPublicKeyFromPrivateKey(Ed25519privSignJwk, "ed25519");

    expect(publicKey.x).to.equal(Ed25519privSignJwk.x);
  });

  it("should throw when presented unsupported curve", () => {
    expect(() => {
      getPublicKeyFromPrivateKey(Ed25519privSignJwk, "NOTACURVE");
    }).to.throw();
  });

});
