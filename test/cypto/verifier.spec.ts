import { expect } from "chai";

import { ed25519Verifier, secp256k1Verifier } from "../../src/crypto/verifier";
import {
  Ed25519pubSignJwk,
  Ed25519signature,
  ES256KpubSignJwk,
  ES256Ksignature,
  message
} from "../resources/testData";

describe("Verifier", () => {
  it("can Verify a valid secp265k1 signature", () => {
    const verified = secp256k1Verifier.verify(
      { x: ES256KpubSignJwk.x, y: ES256KpubSignJwk.y },
      message,
      ES256Ksignature
    );
    expect(verified);
  });

  it("can Verify a valid Ed25519 signature", () => {
    const verified = ed25519Verifier.verify(
      { x: Ed25519pubSignJwk.x },
      message,
      Ed25519signature
    );
    expect(verified);
  });
});
