import base64url from "base64url";
import { Buffer } from "buffer";

import { expect } from "chai";
import { Ed25519privSignJwk, ES256KprivSignJwk, JWK_ARTHUR_FULL_X25519, JWK_FORD_FULL_X25519 } from '../resources/testData';

import { deriveKeyFromSecret, deriveSharedSecret, getPublicKeyFromPrivateKey } from "../../src/crypto/keys";

describe("Keys", () => {
  describe("getPublicKeyFromPrivateKey", () => {
    it("Can get PublicKey from secp256k1 PrivateKey", () => {
      const publicKey = getPublicKeyFromPrivateKey(ES256KprivSignJwk, "secp256k1");

      expect(publicKey.x).to.equal(ES256KprivSignJwk.x);
      expect(publicKey.y).to.equal(ES256KprivSignJwk.y);
    });

    it("Can get PublicKey from Ed25519 PrivateKey", () => {
      const publicKey = getPublicKeyFromPrivateKey(Ed25519privSignJwk, "ed25519");

      expect(publicKey.x).to.equal(Ed25519privSignJwk.x);
    });
  });

  it("Can correctly perform a simulated secret exchange", async () => {
    const keypair1 = {
      d: "U7ZBkzSFDI7uUV0RfDmFaeHxUCnFaxaib9sypeLrJ-M",
      x: "U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24"
    };
    const keypair2 = {
      d: "vfoxXOfEJsZXwzgOVLEK7kmqgcaX0gXtYaEFD2tlwbA",
      x: "kVNmr6NqCs1mjtprt4sK01Is6J39jpprRz4Ky3Ig-1M"
    };

    const keypair1SharedSecret = await deriveSharedSecret(
      { d: keypair1.d },
      { x: keypair2.x }
    );
    const keypair2SharedSecret = await deriveSharedSecret(
      { d: keypair2.d },
      { x: keypair1.x }
    );
    expect(keypair1SharedSecret).to.deep.equal(keypair2SharedSecret);
    const expected = new Uint8Array(
      [
        86, 249, 186, 66, 4, 204, 2, 67, 8, 232, 187, 35, 18, 146, 152, 166,
        51, 193, 177, 31, 159, 106, 130, 232, 207, 255, 93, 2, 82, 55, 222, 230
      ]
    );
    expect(keypair1SharedSecret).to.deep.equal(expected);
  });

  it("Can derive A256GCM key, from a shared secret generated from a DiffieHelman", async () => {

    const sharedSecret = await deriveSharedSecret({ d: JWK_ARTHUR_FULL_X25519.d }, { x: JWK_FORD_FULL_X25519.x });
    const key = await deriveKeyFromSecret(
      sharedSecret,
      Buffer.from('Arthur'),
      Buffer.from('Ford'),
      "C20P"
    );
    const jwk = await crypto.subtle.exportKey("jwk", key);
    expect(base64url.toBuffer(jwk.k!).length).to.equal(32);
    expect(jwk.k!).to.equal("WcqyVAoZ74rDfhaR1np-TXkvE41dszdqdAQ_KAuTYKQ");
  });

  it("Can from a secret, with defaults", async () => {
    const secret = new Uint8Array(
      [
        86, 249, 186, 66, 4, 204, 2, 67, 8, 232, 187, 35, 18, 146, 152, 166,
        51, 193, 177, 31, 159, 106, 130, 232, 207, 255, 93, 2, 82, 55, 222, 230
      ]
    );
    const key = await deriveKeyFromSecret(
      secret
    );
    expect(key).to.be.a("object");
  });

});
