import { expect } from "chai";
import { Ed25519privSignJwk, Ed25519pubSignJwk } from "../resources/testData";

import { createJWT, openJWT } from "../../src/crypto/jwt";

const header = {
  alg: "ES256K",
  typ: "JWT",
};

const payload = {"abc": "123","foo": "bar"};


const JWSArthurDent = "eyJhbGciOiJFZDI1NTE5In0.eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ.Df9K5X1o-oHtS_X3RDZIlDxIH6BMRRkg_eQoA5XZUe-qmhIaPqoabwEHBBR2MMqEbm1SpT7a1s9VwAxZJrE0Bg";
const unverifiableJWT = "eyJhbGciOiJFZDI1NTE5In0.eyJhZHZpY2UiOiJEb24ndCBwYW5pYyEifQ.Df9K5X1o-oHtS_BROKENBROKENBROKEN_eQoA5XZUe-qmhIaPqoabwEHBBR2MMqEbm1SpT7a1s9VwAxZJrE0Bg";
const brokenJWT = "eyJhYmMiOiIxMjMiLCJmb28iOiJiYXIifQ.gJfEOdJLN6YSqfi0LG62LR-8oTd_8l3l4gLdF7kRaiJ6126eI_0jeWJLqtliQH0EWmcqFQfPfaEe7Q8067yVAg";

const hexdocsKey = {
  "crv": "Ed25519",
  "d": "VoU6Pm8SOjz8ummuRPsvoJQOPI3cjsdMfUhf2AAEc7s",
  "kty": "OKP",
  "x": "l11mBSuP-XxI0KoSG7YEWRp4GWm7dKMOPkItJy2tlMM",
};
const hexdocsJWT = "eyJhbGciOiJFZDI1NTE5In0.e30.xyg2LTblm75KbLFJtROZRhEgAFJdlqH9bhx8a9LO1yvLxNLhO9fLqnFuU3ojOdbObr8bsubPkPqUfZlPkGHXCQ";

const externallyGeneratedKey = { kty: "OKP", crv: "Ed25519", x: "38aP7HNVPan9arm-3KaVbB4fOX33zh0CSvokT6qRQgE" }; 
const externallyGeneratedJWT = "eyJhbGciOiJFZDI1NTE5In0.eyJ0eXBlIjoic2hhcmVSZXEiLCJpc3MiOiI0ZWQyYTAwNi1lNzE2LTQzZTYtODNlMy0yMTA1MzcxODZkOTUiLCJpYXQiOjE1NTE4MTUxMjQsImV4cCI6MTU1MTgxNTcyNCwiaXNzYyI6eyJuYW1lIjoiU2luZ2xlU291cmNlIiwicHJvZmlsZUltYWdlIjoiaHR0cHM6Ly9zMy1hcC1zb3V0aGVhc3QtMi5hbWF6b25hd3MuY29tL2lkcnMtaWNvbnMubXlzaW5nbGVzb3VyY2UuaW8vNGVkMmEwMDYtZTcxNi00M2U2LTgzZTMtMjEwNTM3MTg2ZDk1LnBuZyIsIm9ibyI6eyJuYW1lIjoiaXN1cnV0ZXN0IiwicHJvZmlsZUltYWdlIjoiaHR0cHM6Ly9zMy1hcC1zb3V0aGVhc3QtMi5hbWF6b25hd3MuY29tL2lkcnMtaWNvbnMubXlzaW5nbGVzb3VyY2UuaW8vYjdhYjlkYWEtYjQ2Zi00YWNlLWFmMTMtYzFmYzU5OGJhZDVmLnBuZyJ9fSwicmVxdWVzdGVkIjpbInBob3RvIiwiZmlyc3ROYW1lIiwibGFzdE5hbWUiXSwidmVyaWZpZWQiOlsicGhvdG8iXSwiY2FsbGJhY2siOiJodHRwOi8vMTI3LjAuMC4xOjkwMDAiLCJlbmNyeXB0aW9uS2V5Ijp7Imt0eSI6Ik9LUCIsImNydiI6IlgyNTUxOSIsIngiOiJUdkhVbjA5RVFtYUdqT3loaV9YMWNDYWpQVUVXTnUxb1RMVE9CaVloM1Y0In19.k0PjzS62uPs6KVa_nHtSSD_MKMSb5p9lQn5SBxNXXa9yjRjvz8xskOWxyRjObc_MnM7fDApMJhkB0uPzTlXoBQ";

describe("JWT", () => {

  describe("Create JWT", () => {

    it("Can create a JWT", () => {
      const JWT = createJWT(payload, Ed25519privSignJwk.d);
      const parts = JWT.split(".");
      expect(parts[0]).to.equal("eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ");
      expect(parts[1]).to.equal("eyJhYmMiOiIxMjMiLCJmb28iOiJiYXIifQ");
    });

    it("Can create a JWT from String", () => {
      const stringPayload = JSON.stringify(payload);
      const JWT = createJWT(stringPayload, Ed25519privSignJwk.d, {});
      const parts = JWT.split(".");
      expect(parts[0]).to.equal("eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ");
      expect(parts[1]).to.equal("eyJhYmMiOiIxMjMiLCJmb28iOiJiYXIifQ");
    });

    it("Will throw when no Key is provided", () => {
      const shouldThrow = () => {
        createJWT(payload, "");
      };
      expect(shouldThrow).to.throw();
    });

  });

  describe("Open JWT", () => {
    it("can open Arthur Dent's JWT", () => {
      const JWT = openJWT(JWSArthurDent, Ed25519pubSignJwk);
      // expect(JWT.payload).to.deep.equal(payload);
      // expect(JWT.header).to.deep.equal(header);
    });

    it("can open HexDoc's JWT", () => {
      const JWT = openJWT(hexdocsJWT, hexdocsKey);
      // expect(JWT.payload).to.deep.equal(payload);
      // expect(JWT.header).to.deep.equal(header);
    });

    it("can open an externally generated JWT", () => {
      const JWT = openJWT(externallyGeneratedJWT, externallyGeneratedKey);
      // expect(JWT.payload).to.deep.equal(payload);
      // expect(JWT.header).to.deep.equal(header);
    });

    it("should throw when the JWT cannot be verified", () => {
      expect( () => {openJWT(unverifiableJWT, Ed25519pubSignJwk);} ).to.throw();
    });

    it("should throw when the JWT is malformed", () => {
      expect(() => { openJWT(brokenJWT, Ed25519pubSignJwk); }).to.throw();
    });
  });

});
