# KauriID.js
A Javascript library of tools to support working with the KauriID data structures and protocals.

## Installation

### Web
1. Download latest release, place the `kauriid.js` file somewhere acessable to your webserver
2. Drop a script tag in your HTML (to the path you specified in the previous step)
```html
<script src="scripts/kauriid.js">
```

### Node or Webpack
1. Install package.
```
npm install kauriidjs
```
2. Import it in your code.
```javascript
import * as kauriId from 'kauriidjs';
```

## Useage

### Creating a signed JWT
Creating a JWT signed using Ed25519.
```javascript
import { createJWT } from 'kauriidjs';

const payload = {
    "foo": "bar",
    "answer": 42,
}
// Any extra headers to add to the JWT
const extraHeader = { "author": "Mischa" }

// key must be a Base64URL encoded String representation of the secret value (called d) of a Ed25519 keypair;
const key = "aRx7IhkeN6u77hCepIXssdZ3up0rt1Dybj_EOZ8CVhM";

let JWT = createJWT(payload, key, extraHeader);

// JWT = "Header"."Payload"."Signature"
```
### Opening (and verifying) a JWT
Verify and Open a JWT using Ed25519.
Will Error if the JWT is malformed, or cannot be verified.
```javascript
import {openJWT} from 'kauriidjs';

// Public key is an object containing x coordinate from a Ed255191 key, each encoded as Base64URL strings
let publicKey =  {
    'x': 'XNnLyemoMMF54lp-gNsqGtz1g_KInfAyasHQEHgSjIs'
}

let JWT = "eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ.eyJhYmMiOiIxMjMiLCJmb28iOiJiYXIifQ.BaLPsexMidcbiLPikz79aPlnPnyZCtLrKbETPQxbKcWr-9EMuYcC8yhOlbwPhEZU1EvRno7iu271MuSiwdvViA";

let openedJWT = openJWT(JWT, publicKey)
// openedJWT = {
//    header: object,
//    payload: object,
//} 
```

### Creating a ClaimSet
```javascript
import { ClaimSetKeys } from 'kauriidjs';

const myName = {
    "@context": "http://schema.org",
    "@type": "Person",
    "familyName": "Potter",
    "givenName": "Harry"
}

const memberof = {
    "@context": "http://schema.org",
    "@type": "Person",
    memberOf: {
        "@type": "Organization"
        "name": "Hogwarts School of Witchcraft and Wizardry",
        "department": {
            "@type": "Organization",
            "name": "Gryffindor House"
        }
    }
};


// Create a claimset and add some claims
// Each claim will be encrypted, salted and hashed, and stores, with their keys, on the object.
const myClaimSet = new ClaimSetKeys();
await myClaimSet.add(myName);
await myClaimSet.add(memberof);

// Set Signing and Object keys, to allow us to sign and encrypt the Claimset
// Signing key must be a Ed25519 key
myClaimset.signingKey = {d: "aRx7IhkeN6u77hCepIXssdZ3up0rt1Dybj_EOZ8CVhM"}
// Object key must be a native WebCryptoAPI CryptoKey object, with Encrypt and Decrypt KeyOps
const jwk = { alg: "A256GCM",
              ext: true,
              k: "4DV36GqbzLaJ4kaP50zmcyy9NjBPOhhySejSJjhsKII",
              key_ops: ["encrypt, decrypt"],
              kty: "oct"
            };
myClaimSet.objectKey = await window.crypto.subtle.importKey("jwk",jwk,"AES-GCM",true,["encrypt", "decrypt"]);

//Now we can finalise our Claimset
const finalised = await myClaimSet.finaliseClaimSet();
// finalised should now be a very large looking JWE string.
// Hooray!
```

### Displaying a login QR Code
```javascript
import { createQrForRequest } from 'kauriidjs';

const payload = 'This should be a valid JWT returned by SingleSource Request Service';
// A canvas DOM element
const canvas = window.document.body.getElementsByTagName('canvas');
// Size is optional, defaults to a 500 pixels square.
const size = 600;
createQrForRequest(payload, canvas, size);
```
