import base64url from "base64url";
import { Buffer } from "buffer";
import * as elliptic from "elliptic";
import { IPrivateEcKey } from "./keys";

const secp256k1 = new elliptic.ec("secp256k1");
const ed25519 = new elliptic.eddsa("ed25519");

/**
 * Sign message usng secp256k1
 * @param privateKey secp256k1 private key, must contain x and y
 * @param message base64url encoded string to be signed
 * @returns base64url encoded string of 64 bytes, first 32 are R last 32 are S
 */
export let secp256k1Signer: ISigner = {
  sign: (privateKey: IPrivateEcKey, message: string) => {
    const decodedPrivateKey = base64url.decode(privateKey.d, "hex");
    const key = secp256k1.keyFromPrivate(decodedPrivateKey);
    const ecSig = key.sign(message);
    return sigToBase64url(ecSig);
  }
};

/**
 * Sign message usng Ed25519
 * @param privateKey Ed25519 private key, must contain x
 * @param message base64url encoded string to be signed
 * @returns base64url encoded string of 64 bytes, first 32 are R last 32 are S
 */
export let ed25519Signer: ISigner = {
  sign: (privateKey: IPrivateEcKey, message: string) => {
    if (!privateKey || privateKey.d === "" || privateKey.d === undefined) {
      throw new Error("ed25519Signer: private key must be provided to sign");
    }
    const decodedPrivateKey = base64url.decode(privateKey.d, "hex");
    const key = ed25519.keyFromSecret(decodedPrivateKey);
    const sigBuffer = key.sign(message).toBytes();
    return base64url.encode(sigBuffer);
  }
};

/**
 * The Signer interface, describes the sign function
 * @interface ISigner
 */
interface ISigner {
/**
 * @param privateKey an elliptic curve private key
 * @param message base64url encoded string to be signed
 * @returns base64url encoded string of 64 bytes, first 32 are R last 32 are S
 */
  sign(privateKey: IPrivateEcKey, message: string): string;
}

/**
 * convert a EllipticJS signiture to a Base64URL encoded string
 * for ease of transport.
 * First 32 bytes are R, last 32 bytes are S
 */
export const sigToBase64url = (sig: elliptic.ec.Signature): string => {
  const sigBuffer = Buffer.alloc(64);
  sig.r.toBuffer().copy(sigBuffer, 0);
  sig.s.toBuffer().copy(sigBuffer, 32);
  return base64url.encode(sigBuffer);
};
