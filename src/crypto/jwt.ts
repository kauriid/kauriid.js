import base64url from 'base64url';
import { IPublicEcKey } from "./keys";
import { ed25519Signer } from './signer';
import { ed25519Verifier } from './verifier';

/**
 * Verify a JWT string
 * errors if the JWT string is not in the format xxxx.yyyy.zzzz
 * returns false if it cannot verify the JWT with the provided key
 */
function verifyJWT(jwt: string, publicKey: IPublicEcKey) {
  const parts = jwt.split(".");
  if (parts.length !== 3) {
    throw new Error('Incorrect format JWT');
  }
  const message = `${parts[0]}.${parts[1]}`;
  const signature = parts[2];
  return ed25519Verifier.verify(publicKey, message, signature);
}

/**
 * Verify and decode a JWT string.
 * Errors if the JWT cannot be verified.
 * no data will be extracted if the JWT cannot be verified
 *
 * @export
 */
export function openJWT(jwt: string, publicKey: IPublicEcKey): IJWT {
  if (!verifyJWT(jwt, publicKey)) {
    throw new Error("openJWT: JWT must be verifyable to extract data");
  }
  const parts = jwt.split(".");
  return {
    header: JSON.parse(base64url.decode(parts[0])),
    payload: JSON.parse(base64url.decode(parts[1])),
  };
}

/**
 * create a JWT String
 * Currently only signs with secpk256k1
 *
 */
export function createJWT(
  payload: object | string,
  privateKey: string,
  headers: object = {},
): string {
  if (!payload || !privateKey) {
    throw new Error(
      "createJWT: Must provide a Payload and Public key to create a JWT"
    );
  }
  const defaultHeaders = {
    alg: "ES256K",
    typ: "JWT",
  };
  const finalHeaders = JSON.stringify({ ...defaultHeaders, ...headers });
  const headersBase64 = base64url.encode(finalHeaders);
  const payloadString = typeof payload === "string" ?
    payload
    : JSON.stringify(payload);
  const payloadBase64 = base64url.encode(payloadString);
  const message = `${headersBase64}.${payloadBase64}`;
  const signature = ed25519Signer.sign({ d: privateKey }, message);

  return `${headersBase64}.${payloadBase64}.${signature}`;
}

interface IJWT {
  header: object;
  payload: object;
}
