import base64url from "base64url";
import * as elliptic from "elliptic";

import { IPublicEcKey } from "./keys";

const secp256k1 = new elliptic.ec("secp256k1");
const ed25519 = new elliptic.eddsa("ed25519");

/**
 * Verify a message using secp256k1
 * @param message base64url encoded message to be verified
 * @param publicKey ox and y value required
 * @param signature base64url encoded signature to verify the message
 * @returns True if it manages to verify, false if it does not
 */
export let secp256k1Verifier: IVerifier = {
  verify: (
    publicKey: IPublicEcKey,
    message: string,
    signature: string
  ): boolean => {
    const x = base64url.decode(publicKey.x, "hex");
    const y = base64url.decode(publicKey.y as string, "hex");
    const key = secp256k1.keyFromPublic({ x, y });

    const sigbuffer = base64url.toBuffer(signature);

    const rBuffer = sigbuffer.slice(0, 32);
    const sBuffer = sigbuffer.slice(32);

    return secp256k1.verify(message, { r: rBuffer, s: sBuffer }, key);
  }
};

/**
 * Verify a message using ed25519
 * @param message base64url encoded message to be verified
 * @param publicKey only needs x value
 * @param signature base64url encoded signature to verify the message
 * @returns True if it manages to verify, false if it does not.
 */
export let ed25519Verifier: IVerifier = {
    verify: (
    publicKey: IPublicEcKey,
    message: string,
    signature: string
  ): boolean => {
    const keyBytes = base64url.decode(publicKey.x, "hex");
    const key = ed25519.keyFromPublic(keyBytes);
    const messageBytes = Buffer.from(message);

    const sighex = base64url.decode(signature, "hex");
    return key.verify(messageBytes, sighex);
  }
};

/**
 * Verifier object, describes the verify function
 * @param message base64url encoded message to be verified
 * @param publicKey
 * @param signature base64url encoded signature to verify the message
 * @returns True if it manages to verify, false if it does not.
 *
 * @interface IVerifier
 */
interface IVerifier {
  verify(publicKey: IPublicEcKey, payload: string, signature: string): boolean;
}

