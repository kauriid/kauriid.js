let cachedCrypto: object | undefined;

/**
 * Get the crypto object of the environment we are running under (Web or Node)
 */
export const getCrypto = (): any => {
  const globalObject = getGlobal();
  if (cachedCrypto) {
    return cachedCrypto;
  }

  if (typeof (globalObject as any).crypto !== "undefined") {
    if (!(globalObject as any).crypto.subtle) {
      (globalObject as any).crypto.subtle =
        (globalObject as any).crypto.webkitSubtle;
    }
    return (globalObject as any).crypto;
  }
};

/**
 * Override the Crypto object, so the user can set the Crypto object to be used
 * in an environment where they may have a choice and the default is bad.
 *
 * @param crypto the crypto object to be be used instead of the one on the
 *   global oject.
 */
export const setCryptoObject = (crypto: object) => {
  cachedCrypto = crypto;
};

/**
 * Get the Global object of the environment we are running under (Web or Node)
 */
export const getGlobal = () => {
  const globalObject = typeof window !== "undefined" ? window : global;
  return globalObject;
};

/**
 * create an array of randomly ordered elements of 0 to length
 *
 * @param length the length of the order array to return
 */
export const getRandomOrder = (length: number): number[] => {
  const crypto = getCrypto();
  const array1 = crypto.getRandomValues(new Uint8Array(length));
  const array2 = Array.from(array1.slice().sort());
  const order: number[] = array2.map((elem) => array1.indexOf(elem));
  return order;
};
