import base64url from "base64url";
import { Buffer } from "buffer";
import * as elliptic from "elliptic";
import _sodium from "libsodium-wrappers";
import { getCrypto } from "../crypto/utils";

const secp256k1 = new elliptic.ec("secp256k1");
const ed25519 = new elliptic.eddsa("ed25519");

/**
 * Derive a shared secret from our private DH key,
 *  and our recipient's public DH key
 *
 * @param ourDHKey our private HD key
 * @param recipientDHKey our recipient's public DH key
 */
export const deriveSharedSecret = async (
  ourDHKey: IPrivateEcKey,
  recipientDHKey: IPublicEcKey
): Promise<Uint8Array> => {
  await (_sodium.ready);
  const sodium = _sodium;
  const decodedPrivateKey = sodium.from_base64(ourDHKey.d);
  const decodedPublicKeyX = sodium.from_base64(recipientDHKey.x);
  return sodium.crypto_box_beforenm(decodedPublicKeyX, decodedPrivateKey);
};

/**
 * Derive an 256 bit AES-GCM key from a buffer of sufficient size
 *
 * @param sharedSecret buffer containing bytes from which to derive
 *   a A256GCM key
 * @param apu key agreement PartyUInfo
 * @param apv key agreement PartyVInfo
 * @param enc AlgorithmID of the key to be derived
 */
export const deriveKeyFromSecret = async (
  sharedSecret: Uint8Array,
  apu: Uint8Array = new Uint8Array([]),
  apv: Uint8Array = new Uint8Array([]),
  enc: string = "A256GCM"
): Promise<CryptoKey> => {
  const roundNumber = new Uint8Array([0, 0, 0, 1]);
  const encBuffer = Buffer.from(enc);
  const encLength = numberToUint32ByteArray(encBuffer.length);
  const apuLength = numberToUint32ByteArray(apu.length);
  const apvLength = numberToUint32ByteArray(apv.length);
  const supPubInfo = numberToUint32ByteArray(256);

  const hashInput = Buffer.alloc(
    4 + // roundNumber
    sharedSecret.length + // sharedSecret
    encBuffer.length + 4 + // enc
    apu.length + 4 + // apu
    apv.length + 4 + // apv
    4 // supPubInfo
  );
  hashInput.set(roundNumber);
  let currentlength = roundNumber.length;
  hashInput.set(sharedSecret, roundNumber.length);
  currentlength += sharedSecret.length;
  hashInput.set(encLength, currentlength);
  currentlength += 4;
  hashInput.set(encBuffer, currentlength);
  currentlength += encBuffer.length;
  hashInput.set(apuLength, currentlength);
  currentlength += 4;
  hashInput.set(apu, currentlength);
  currentlength += apu.length;
  hashInput.set(apvLength, currentlength);
  currentlength += 4;
  hashInput.set(apv, currentlength);
  currentlength += apv.length;
  hashInput.set(supPubInfo, currentlength);
  currentlength += supPubInfo.length;

  const crypto = getCrypto();
  const hash: ArrayBuffer = await crypto.subtle.digest("SHA-256", hashInput);
  const derivedkey = hash.slice(0, 32);

  const key = await crypto.subtle.importKey(
    "raw",
    derivedkey,
    { name: "AES-GCM", length: 256 },
    true,
    ["encrypt", "decrypt"]
  );

  return key;
};

/**
 * derive the public key from a private key
 * @param curve the name of the curve that this key is from
 *   either secp256k1 or ed25519
 * @param privateKey the private key to derive the public key from
 */
export const getPublicKeyFromPrivateKey = (
  privateKey: IPrivateEcKey,
  curve: string
): IPublicEcKey => {
  let ellipticPublicKey: any;
  const decodedPrivateKey = base64url.decode(privateKey.d, "hex");
  switch (curve.toLowerCase()) {
    case "secp256k1":
      ellipticPublicKey =
        secp256k1.keyFromPrivate(decodedPrivateKey).getPublic();
      break;
    case "ed25519":
      ellipticPublicKey =
        ed25519.keyFromSecret(decodedPrivateKey).getPublic("hex");
      break;
    default: throw new Error("getPublicKeyFromPrivateKey: unsupported curve");
  }
  let y: string | null = null;
  let x: string;
  let crv: "secp256k1" | "Ed25519";
  if (curve.toLowerCase() === "secp256k1") {
    x = base64url.encode(ellipticPublicKey.getX().toString(16), "hex");
    y = base64url.encode(ellipticPublicKey.getY().toString(16), "hex");
    crv = "secp256k1";
  } else {
    x = base64url.encode(ellipticPublicKey, "hex");
    crv = "Ed25519";
  }

  return {
    crv,
    x,
    y: y ? y : undefined,
  };
};

/**
 * convert a Number to a 4 byte array, representing a unsigned 32 bit integer
 * resulting array is in big-endian format.
 * 
 * @param input the number to be converted to an uint32 byte aray
 */
const numberToUint32ByteArray = (input: number): Uint8Array => {
  /* istanbul ignore if */
  if (input < 0 || input > 4294967295) {
    throw new Error("numberToUint32ByteArray:" +
      " Number provided is outside the bounds on a Uint32");
  }
  const byteArray = new Uint8Array(4);
  for (let index = 0; index < byteArray.length; index++) {
    // tslint:disable-next-line: no-bitwise
    byteArray[3 - index] = input & 0xff;
    // tslint:disable-next-line: no-bitwise
    input = input >> 8;
  }
  return byteArray;
};

/**
 * An Elliptic Curve Public key.
 * x and y are a coordinate pair on a curve, each encoded as Base64URL strings
 * y can be omitted, if the curve allows for it
 *
 * @interface IPublicEcKey
 */
export interface IPublicEcKey {
  crv?: string;
  x: string;
  y?: string;
}

/**
 * Representation of a Elliptic Curve Private key
 * d must be a secret number for a ECC point, encoded as a Base64URL string
 *
 * @export
 * @interface IPrivateEcKey
 */
export interface IPrivateEcKey {
  d: string;
}
