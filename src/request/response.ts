/**
 * Created: 2019-3-27 Mischa A. MacLeod <mischa@mysinglesource.io>
 *
 * (c) 2018 - 2019 by SingleSource Limited, Auckland, New Zealand
 *     http://mysinglesource.io/
 *     Apache 2.0 Licence.
 *
 * This work is licensed under the Apache 2.0 open source licence.
 * Terms and conditions apply.
 *
 * You should have received a copy of the license along with this program.
 */

import { Attestation } from "../claims/attestation";
import { IClaimKeys } from "../claims/claimSetKeys";
import { defaultResponseTtl } from "../constants";
import { getCrypto } from "../crypto/utils";

/**
 * Create a Response to a Selective Request for disclosure from one or
 *   many Attestations.
 *
 * @param iss the DID of the issuer of this response
 * @param requestJWT the request we are responding to, in verbatim as a JWT
 * @param attestations Attestations we are picking claims from to create
 *   this response
 * @param objectKeys Array of keys to use to open the provided attestations,
 *  in the same order as the Attestations array
 * @param claimKeys Array of objects containing the keys to the claims we which
 *  to provide to the requester
 */
export const buildResponse = async (
  iss: string,
  requestJwt: string,
  attestations: Attestation[],
  objectKeys: CryptoKey[],
  claimKeys: IClaimKeys[],
): Promise<ISelectiveDisclosureResponse> => {
  const attestationKeysPromises: Array<Promise<IAttestationKeys>> = 
    attestations.map( async (attestation, index) => {
      const thisObjectKey = objectKeys[index];
      const finalisedAttestationPromise = attestation.finalise(thisObjectKey);
      const objectKeyPromise =
        getCrypto().subtle.exportKey("jwk", thisObjectKey);
      const thisClaimKeys = claimKeys[index];
      const finalisedAttestation = await finalisedAttestationPromise;
      const objectKey = await objectKeyPromise;
    return {
      attestation: finalisedAttestation,
      claimKeys: thisClaimKeys,
      objectKey,
    };
  });
  const attestationKeys = await Promise.all(attestationKeysPromises);
  return {
    exp: ((new Date).getTime() / 1000) + defaultResponseTtl,
    iat: (new Date).getTime() / 1000,
    iss,
    requestJwt,
    vc: attestationKeys
  };
};

interface IAttestationKeys {
  attestation: string; // JWE
  claimKeys: IClaimKeys;
  objectKey: JsonWebKey;
}

interface ISelectiveDisclosureResponse {
  requestJwt: string;
  vc: IAttestationKeys[];
  exp: number;
  iat: number;
  iss: string;
}
