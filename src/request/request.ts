/**
 * Created: 2018-12-6 Mischa A. MacLeod <mischa@mysinglesource.io>
 *
 * (c) 2018 by SingleSource Limited, Auckland, New Zealand
 *     http://mysinglesource.io/
 *     Apache 2.0 Licence.
 *
 * This work is licensed under the Apache 2.0 open source licence.
 * Terms and conditions apply.
 *
 * You should have received a copy of the license along with this program.
 */

import QRious from "qrious";

import { pollForResponse } from "./messageServiceGateway";

/**
 * Creates a QR code for the provided payload, and surfaces it on the DOM in
 * the specified element.
 *
 * @export
 * @param {string} payload string to be encoded into QR code
 * @param {Element} element canvas or Image element to display the QR code in
 * @param {number} [size=500] desired size of the QR code
 *  Note: it is possible to create a QR that is too small to read
 * @returns {Element} the altered and surfaced element
 */
export function createQrForRequest(
  payload: string,
  element: Element,
  size: number = 500,
): Element {
  const qr = new QRious({
    element,
    level: "M",
    size,
    value: payload,
  });
  return element;
}

/**
 * Hits the SingleSource API to get a payload for a Request.
 *
 * @export
 * @param {object} params parameters of the Request
 * @returns {string} a requst object, signed by SingleSource then stringified
 */
export function fetchRequestPayload(params: object): string {
  // fetch request payload
  return "";
}
