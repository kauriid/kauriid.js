/**
 * Created: 2018-12-6 Mischa A. MacLeod <mischa@mysinglesource.io>
 *
 * (c) 2018 by SingleSource Limited, Auckland, New Zealand
 *     http://mysinglesource.io/
 *     Apache 2.0 Licence.
 *
 * This work is licensed under the Apache 2.0 open source licence.
 * Terms and conditions apply.
 *
 * You should have received a copy of the license along with this program.
 */

import R from "ramda";
import { HttpError } from "../types/HttpError";

/**
 * Constantly polls the provided topicUri
 * resolving the returned promise only when the
 * topicBox returns a non-empty message
 *
 * @export
 * @param {string} topicUri uri of the topic on the message service
 * @returns {Promise<object>}
 */
export async function pollForResponse(
  topicUri: string
): Promise<object | undefined> {
  let response: object | undefined;
  let failCount = 0;
  while (response === undefined && failCount < 5) {
    try {
      const returned = await getTopic(topicUri);
      if (R.isEmpty(returned)) {
        await timeout(2000);
      } else {
        response = returned;
        failCount = 0;
      }
    } catch (error) {
      failCount++;
    }
  }
  if (failCount > 4) {
    throw new Error("Failed to contact message service five times");
  }
  return response;
}

/**
 * Fetch a topic from the message service.
 *
 * @param {string} topicUri uri of the topic on the message service
 * @returns {Promise<object>} the message payload
 */
async function getTopic(topicUri: string): Promise<object> {
  const response = await window.fetch(topicUri, { method: "GET" });
  if (response.status !== 200) {
    throw new HttpError(
      "Request to Message Service failed",
      response.status,
      response.statusText
    );
  }

  const json: any = await response.json();
  const message = json.message;
  return message;
}

/**
 * Creates a promise that is resolved after the specified number
 *   of milliseconds, basically a promisified version of setTimeout
 *   because callbacks are icky
 *
 * @param {number} ms time to wait in ms
 * @returns
 */
function timeout(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
