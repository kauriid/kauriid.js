/**
 * Created: 2018-12-6 Mischa A. MacLeod <mischa@mysinglesource.io>
 *
 * (c) 2018 by SingleSource Limited, Auckland, New Zealand
 *     http://mysinglesource.io/
 *     Apache 2.0 Licence.
 *
 * This work is licensed under the Apache 2.0 open source licence.
 * Terms and conditions apply.
 *
 * You should have received a copy of the license along with this program.
 */

export const messageServiceUrl = "";
export const defaultTtlSpan = 365 * 24 * 3600;  // 1 year in seconds.;
export const defaultResponseTtl = 600; // 10 minutes