import base64url from "base64url";
import { Jose } from "jose-jwe-jws";
import R from "ramda";
import timingSafeEqual from "timing-safe-equal";

import { saltHashClaim } from "../claims/claim";
import { createJWT } from "../crypto/jwt";
import {
  getPublicKeyFromPrivateKey,
  IPrivateEcKey,
  IPublicEcKey
} from "../crypto/keys";
import { ed25519Verifier } from "../crypto/verifier";
import { ClaimSetKeys, ICommitent, openClaimSet } from "./claimSetKeys";

/**
 * Object representing a Deserialised Attestation object, before finalisation
 *
 * @export
 * @class Attestation
 */
export class Attestation {
  public claimSet: ClaimSetKeys;
  public commitments: { [role: string]: string };
  private content: string;
  private attesterCommitmentHashes: Buffer[];
  private attesterCommitmentSalts: Buffer[];
  private subject: string;

  /**
   * Create an Attestation Instance,
   * Setting the ClaimSet to be attested and an Attestor's Commitment
   *  (Either Foreign or Self Signed)
   * @param claimSet the ClaimSetKeys object that the attestation is to refer
   * @param attestorCommitment the Attestors commitment to add to
   *   the attestation
   * @memberof Attestation
   */
  constructor(
    claimSet: ClaimSetKeys,
    content: string,
    attestorCommitment: ICommitent,
    signedAttesterCommitment: string,
  ) {
    this.claimSet = claimSet;
    this.content = content;
    this.commitments = { attester: signedAttesterCommitment };
    this.subject = claimSet.subject;

    const commitmentParts = attestorCommitment.commitment.split(".");
    const splitCommitmentElements = R.splitEvery(2, commitmentParts);
    const commitmentSalts = R.pluck(0, splitCommitmentElements);
    const commitmentHashes = R.pluck(1, splitCommitmentElements);
    this.attesterCommitmentHashes = commitmentHashes.map(hash =>
      base64url.toBuffer(hash)
    );
    this.attesterCommitmentSalts = commitmentSalts.map(salt =>
      base64url.toBuffer(salt)
    );
  }

  /**
   * Decrypt the claim at the specified index, then validate it against the
   * attestors commitment
   * errors if the claim cannot be decrypted, or the hashes dont match
   *
   * @param index the index of the claim to access on this attestation's
   *   claimset
   * @param claimKey symmetric key to decrypt to the claim
   * @returns the decrypted claim
   * @memberof Attestation
   */
  public async accessClaim(index: number, claimKey: CryptoKey) {
    const claim = await this.claimSet.accessClaim(index, claimKey);
    const isValid = await this.checkClaimHash(claim, index);
    if (!isValid) {
      throw new Error("accessClaim: claim's hash was invalid," +
        "claim may have been modified");
    }
    return claim;
  }

  /**
   * Counter sign a commitment
   * Create a subject commitment and add it to the commitments
   *  object of the attestation
   *
   * @param signingKey Ed25519 private key to sign the commitment
   * @memberof Attestation
   */
  public cosign(signingKey: IPrivateEcKey) {
    const commitment = this.buildCommitment(signingKey, "subject");
    Object.assign(this.commitments, { "subject": commitment });
  }

  /**
   * Serialise, sign and encrypt the attestation, ready for transport
   *
   * @param objectKey the key to be used to encrypt the Attestation
   * @returns the resulting JWE from finalising the Attestation
   * @memberof Attestation
   */
  public async finalise(objectKey: CryptoKey): Promise<string> {
    const finalAttestation = {
      claimset: this.claimSet,
      commitments: this.commitments,
      content: this.content,
    };
    const finalAttestationString = JSON.stringify(finalAttestation);
    const cryptographer = new Jose.WebCryptographer();
    cryptographer.setKeyEncryptionAlgorithm("dir");
    cryptographer.createIV();
    const encrypter = new Jose.JoseJWE.Encrypter(cryptographer, objectKey);
    const encryptedAttestation =
      await encrypter.encrypt(finalAttestationString);
    return encryptedAttestation;
  }

  /**
   * Build a commitment, using the salts and hashes from the attester
   * commitment but signed with the provided key
   * @param signingKey
   * @returns signed JWT of an commitment create for this attestation with the
   *  given key
   * @memberof Attestation
   */
  private buildCommitment(
    signingKey: IPrivateEcKey,
    role: "subject" | "attester"
  ): string {
    const stringArray: string[] = [];
    const hashes = this.attesterCommitmentHashes;
    const salts = this.attesterCommitmentSalts;
    const saltAndHash = R.zip(salts, hashes);
    const commitmentElements = R.reduce(
      (acc, saltyHash) => {
        acc.push(base64url.encode(saltyHash[0]));
        acc.push(base64url.encode(saltyHash[1]));
        return acc;
      },
      stringArray,
      saltAndHash
    );
    const commitmentString = base64url.encode(commitmentElements.join("."));

    const committerPublicKey = getPublicKeyFromPrivateKey(
      signingKey,
      "ed25519",
    );
    const commitment = {
      commitment: commitmentString,
      committerPublicKey,
      iss: "", // TODO: pass in issuer
      role: "subject",
      sub: this.subject
    };
    const signedCommitment = createJWT(
      commitment,
      (signingKey as IPrivateEcKey).d
    );
    return signedCommitment;
  }

  /**
   * ReSalt and hash the provided claim 
   * and check it against the hash recrvered from the attestor's commitment
   *
   * @private
   * @param claim the claim object to verify
   * @param index the index of the claim to verify
   * @returns promise containing True if the claim verified against
   *   the stored hash
   * @memberof Attestation
   */
  private async checkClaimHash(
    claim: object,
    index: number
  ): Promise<boolean> {
    const origionalSalt = this.attesterCommitmentSalts[index];
    const origionalHash = this.attesterCommitmentHashes[index];
    const saltedHashedClaim = await saltHashClaim(claim, origionalSalt);
    const result = timingSafeEqual(
      Buffer.from(saltedHashedClaim.saltedHash),
      origionalHash
    );
    return result;
  }
}

/**
 * Decrypt attestation, verify Attestor's commitment, deserialise the contained
 * ClaimSet.
 *
 * @param encryptedAttestation serialised and encrypted attestation to open,
 *   as a string
 * @param objectKey key used to decrypt the attestation
 * @param signerKey Attestor's public key, used to verify attestor's commitment
 */
export const openAttestation = async (
  encryptedAttestation: string,
  objectKey: CryptoKey,
  signerKey: IPublicEcKey
) => {
  const headerBase64 = encryptedAttestation.split(".")[0];
  const headerObject = JSON.parse(base64url.decode(headerBase64));
  if (!validateHeaders(headerObject)) {
    throw new Error("openAttestation: Could not validate headers of JWE");
  }
  // Decrypt Attestation
  const cryptographer = new Jose.WebCryptographer();
  const decrypter = new Jose.JoseJWE.Decrypter(cryptographer, objectKey);
  const decryptedAttestation = await decrypter.decrypt(encryptedAttestation);
  const attestation: IDecryptedAttestation = JSON.parse(decryptedAttestation);
  // Check Attestor commitment
  if (!validateCommitment(
    attestation.commitments.attester,
    signerKey
  )) {
    throw new Error("openAttestation: failed to validate" +
    " attestor's commitment");
  }
  // Open and store commitment elements
  const attestorCommitment = openCommitment(attestation.commitments.attester);
  // Deserialise Claimset
  const claimSet = await openClaimSet(
    attestation.claim_set,
    objectKey,
    signerKey
  );
  // Create Attestation object
  const attestationObject = new Attestation(
    claimSet,
    attestation.content,
    attestorCommitment,
    attestation.commitments.attester
  );
  return attestationObject;
};

/**
 * Checks the validity of a commitment JWS
 *
 * @param commitment a commitment object, as a JWS string
 * @param signerKey the public key to check against the commitment
 */
const validateCommitment = (
  commitment: string,
  signerKey: IPublicEcKey
): boolean => {
  const parts = commitment.split(".");
  const message = `${parts[0]}.${parts[1]}`;
  return ed25519Verifier.verify(signerKey, message, parts[2]);
};

/**
 * Check a JWE Headers object, to make sure it's something we we expect,
 *  and can process.
 * 
 * @param headers the serialised Headers object from a JWE
 * @returns true if all tests pass, false if not
 */
const validateHeaders = (headers: object): boolean => {
  const testResults = R.has("alg")(headers) &&
    (headers as any).alg === "dir" &&
    R.has("enc")(headers) &&
    (headers as any).enc === "A256GCM";
  return testResults;
};

/**
 * Create a Commitment object from a commitment JWS
 *
 * @param commitment commitment object as a JWS string
 * @returns commitment object extracted from the JWS
 */
const openCommitment = (commitment: string): ICommitent => {
  const payload = commitment.split(".")[1];
  const deserialisedCommitment: ICommitent =
    JSON.parse(base64url.decode(payload));
  return deserialisedCommitment;
};

/**
 * Object representing a Attestation after decryption and deserialisation
 *
 * @interface IDecryptedAttestation
 */
interface IDecryptedAttestation {
  claim_set: string; // JWE
  commitments: { [role: string]: string }; // values are JWS
  content: string; // JWS
  attestor_commitment: string; // JWS
}

/**
 * Object representing the Signed Content of an Attestation
 *
 * @interface ISignedAttestationContent
 */
export interface ISignedAttestationContent {
  iss: string;
  iat: number;
  claim_set: string; // JWS of a ClaimSet
  provenance: object; // ignore for now
  statements: object; // ignore for now
  ancestor_keys: object; // ignore for now
}

/**
 * Object representing the Commitment object of an Attestation
 *
 * @interface ICommitment
 */
interface ICommitment {
  commitment: string; // salt.hash.salt.hash...
  sub: string;
  iss: string;
  role: string;
}
