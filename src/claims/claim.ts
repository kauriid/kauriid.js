import { Buffer } from 'buffer';
import { Jose } from "jose-jwe-jws";
import { getCrypto } from "../crypto/utils";

/**
 * object containing an encrypted Claim, and it's symmetric key
 *
 * @interface IEncryptedClaim
 */
interface IEncryptedClaim {
  ciphertextClaim: string;
  key: CryptoKey;
}

/**
 * Create an encrypted claim from an object
 *
 * @param plainTextClaim the claim object to be encrypted
 * @returns an object containing the cyphertext and the
 *   newly generated symmetric key
 */
export const encryptClaim = async (
  plainTextClaim: object
): Promise<IEncryptedClaim> => {
  const alg = { name: "AES-GCM", length: 256 };
  const key = await getCrypto().subtle.generateKey(
    alg,
    true,
    ["encrypt", "decrypt"]
  );
  const jsonClaim = JSON.stringify(plainTextClaim);
  const cryptographer = new Jose.WebCryptographer();
  cryptographer.setKeyEncryptionAlgorithm("dir");
  const encrypter = new Jose.JoseJWE.Encrypter(cryptographer, key);
  const jwe = await encrypter.encrypt(jsonClaim);

  return {
    ciphertextClaim: jwe,
    key
  };

};

/**
 * Decrypt a claim using it's symmetric key
 *
 * @param encryptedClaim the Encrypted claim to be decrypted
 * @param key the key to decrypt the claim
 */
export const decryptClaim = async (
  encryptedClaim: string,
  key: CryptoKey,
  decode: boolean = true
): Promise<object | string> => {
  const cryptographer = new Jose.WebCryptographer();
  const decrypter = new Jose.JoseJWE.Decrypter(cryptographer, key);
  const serialised = await decrypter.decrypt(encryptedClaim);

  const claim = JSON.parse(serialised);
  return claim;
};

/**
 * Sstringify the passed in claim, generate a salt, apply it to the claim,
 *   and generate a hash fromthe resulting string.
 * return both the Salt and the Hash in an object
 * 
 * @param claim the claim object to be hashed
 * @returns an object containing a salt and a hash of the inpput claim
 */
export const saltHashClaim = async (
  claim: object,
  inputSalt?: Buffer
): Promise<ISaltedHash> => {
  const crypto = getCrypto();
  const salt: Uint8Array = inputSalt ?
    inputSalt
    : crypto.getRandomValues(new Uint8Array(16));
  const jsonClaim = JSON.stringify(claim);
  const claimBuffer = Buffer.from(jsonClaim);

  const saltedClaimBuffer = new Uint8Array(
    salt.byteLength + claimBuffer.length
    );
  saltedClaimBuffer.set(salt, 0);
  saltedClaimBuffer.set(claimBuffer, salt.byteLength);

  const saltedHash: ArrayBuffer =
    await crypto.subtle.digest("SHA-256", saltedClaimBuffer);

  return {
    salt,
    saltedHash
  };
};

/**
 * A Salted hash with its associated salt
 *
 * @interface ISaltedHash
 */
interface ISaltedHash {
  salt: Uint8Array;
  saltedHash: ArrayBuffer;
}
