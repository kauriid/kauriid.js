import base64url from "base64url";
import { Buffer } from "buffer";
import { Jose } from "jose-jwe-jws";
import R from "ramda";

import { defaultTtlSpan } from "../constants";
import { createJWT } from "../crypto/jwt";
import {
  getPublicKeyFromPrivateKey,
  IPrivateEcKey,
  IPublicEcKey
} from "../crypto/keys";
import { getCrypto, getRandomOrder } from "../crypto/utils";
import { ed25519Verifier } from "../crypto/verifier";
import { decryptClaim, encryptClaim, saltHashClaim } from "./claim";

/**
 * object containing a unserialised Claimset object, and it's keys
 *
 * @export
 * @class ClaimSetKeys
 */
export class ClaimSetKeys {
  // The keys for the claims in this ClaimSet
  public claimKeys: IClaimKeys;
  // The key to be used to sign this ClaimSet on finalisation
  public signingKey?: IPrivateEcKey;
  // The key used to encrypt the ClaimSet on finalisation
  public objectKey?: CryptoKey;
  // Issued At timestamp
  public iat?: number;
  // Expiry timestamp
  public exp?: number;
  // The Encrypted claims
  public claimSet: string[];
  // The suubject of this ClaimSet
  public subject: string;
  // The salts usued to hash each of the claims in the ClaimSet
  public claimSalts: Uint8Array[];
  // The salted and hashed claims of the ClaimSet
  public claimHashes: ArrayBuffer[];

  /**
   * Creates an instance of ClaimSetKeys.
   *
   * @param subject the name of the entity that the claimset pertains to
   * @param [objectKey] the Encryption key used to encrypt the claimset
   * @param [signingKey] the signing key used to sign the Commitment 
   * @returns An new, empty ClaimSetKeys object
   * @constructor
   * @memberof ClaimSetKeys
   */
  constructor(
    subject: string,
    objectKey?: CryptoKey,
    signingKey?: IPrivateEcKey
  ) {
    this.subject = subject;
    this.objectKey = objectKey;
    this.signingKey = signingKey;
    this.claimSet = new Array<string>();
    this.claimKeys = {};
    this.claimSalts = new Array<Uint8Array>();
    this.claimHashes = new Array<ArrayBuffer>();
  }

  /**
   * Add a new claim to the claimset, storing the encrypted claim,
   * random Salt, and Salted Hash of the claim.
   *
   * @param claim JSON-LD object, containing one or more fields of PII
   * @memberof ClaimSetKeys
   */
  public async addClaim(claim: object) {
    const encryptedClaim = await encryptClaim(claim);
    const { salt, saltedHash } = await saltHashClaim(claim);
    const claimIndex = this.claimSet.push(encryptedClaim.ciphertextClaim) - 1;
    this.claimKeys[claimIndex] = encryptedClaim.key;
    this.claimHashes.push(saltedHash);
    this.claimSalts.push(salt);
  }

  /**
   * Finalise the Claimset
   * produce a subject commitment and encrypt the claimset
   *   using the provided object key
   *
   * @param retainOrder specify if the claims order should be shuffled
   * @returns a JWE string, containing the claimset
   * @memberof ClaimSetKeys
   */
  public async finaliseClaimSet(
    retainOrder: boolean = false
  ): Promise<string> {
    if (retainOrder === false) {
      const order = getRandomOrder(this.claimSet.length);
      this.claimSet = order.map(index => this.claimSet[index]);
      this.claimKeys = order.map(index => this.claimKeys[index]);
      this.claimHashes = order.map(index => this.claimHashes[index]);
      this.claimSalts = order.map(index => this.claimSalts[index]);
    }
    const metadata: IClaimSetMetadata = this.createMetadata("subject");
    const finalClaimSet = {
      claims: this.claimSet,
      commitment: metadata.commitment,
      exp: metadata.exp,
      iat: metadata.iat,
      subject: this.subject
    };
    if (this.objectKey === undefined) {
      throw new Error("object key is required to encrypt a commitment");
    }
    const jsonClaimSet = JSON.stringify(finalClaimSet);
    const cryptographer = new Jose.WebCryptographer();
    cryptographer.setKeyEncryptionAlgorithm("dir");
    const encrypter = new Jose.JoseJWE.Encrypter(
      cryptographer,
      this.objectKey
    );
    const jwe = await encrypter.encrypt(jsonClaimSet);
    return jwe;
  }

  /**
   * Decrypt the claim at the given index with the given key
   *
   * @param index the index of the claim to access
   * @param claimKey the key of the claim to access
   * @param rawBytes switch to return a Buffer of the claim,
   *   instead of an object
   * @returns the decrypted claim as an object
   *   or a Buffer, if rawBytes flag is true
   * @memberof ClaimSetKeys
   */
  public async accessClaim(
    index: number,
    claimKey: CryptoKey,
    rawBytes: boolean = false
  ): Promise<object | Buffer> {
    if (index > this.claimSet.length) {
      throw new Error(
        "accessClaim: Requested claim index is outside of ClaimSet Array"
      );
    }
    const claim = this.claimSet[index];
    const decryptedClaim = await decryptClaim(claim, claimKey, !rawBytes);
    let result: object | Buffer;
    result = rawBytes
      ? Buffer.from(JSON.stringify(decryptedClaim))
      : (decryptedClaim as object);
    return result;
  }

  /**
   * Decrypt all claims in a claimset, using the given array of keys
   * Keys array must be in the same order as the Claims in the Claimset
   *
   * @param keys the array of CryptoKeys used to decrypt the claims
   * @retuens Array of the resulting objects from the accessed claims 
   * @memberof ClaimSetKeys
   */
  public async accessAllClaims(keys: CryptoKey[]): Promise<object[]> {
    if (keys.length !== this.claimSet.length) {
      throw new Error(
        "accessAllClaims: Keys must contain all of the Claimset's keys" + 
        " and only the Claimset's keys"
      );
    }
    const claimPromises = keys.map((key, index) => {
      return this.accessClaim(index, key);
    });
    return Promise.all(claimPromises);
  }

  /**
   * create a string of Base64URL encoded Salts and Hashes
   *   concatenated with "."
   *
   * @returns An object containing the commitment String and it's metadata
   * @public
   * @memberof ClaimSetKeys
   */
  public buildCommitment(role: string): ICommitent {
    if (this.signingKey === undefined) {
      throw new Error("Signing key required to create a commitment");
    }

    const stringArray: string[] = [];
    const saltAndHash = R.zip(this.claimSalts, this.claimHashes);
    const commitmentElements = R.reduce(
      (acc, saltyHash) => {
        acc.push(base64url.encode(Buffer.from(saltyHash[0])));
        acc.push(base64url.encode(Buffer.from(saltyHash[1])));
        return acc;
      },
      stringArray,
      saltAndHash
    );
    const commitmentString = commitmentElements.join(".");
    let committerPublicKey = getPublicKeyFromPrivateKey(
      this.signingKey,
      "ed25519"
    );
    committerPublicKey = R.merge(committerPublicKey, { crv: "Ed25519" });
    return {
      commitment: commitmentString,
      committerPublicKey,
      iss: "", // TODO: pass in issuer and role
      role,
      sub: this.subject
    };
  }

  /**
   * Generate metadata for the ClaimSet
   * Requires a SigningKey on this ClaimSetKeys object
   * 
   * @returns an object containing the generated metadata
   * @private
   * @memberof ClaimSetKeys
   */
  private createMetadata(role: string): IClaimSetMetadata {
    const unsignedCommitment = this.buildCommitment(role);
    // we are promising the compiler that the signingKey is defined
    // we checked it earlier.
    const signedCommitment = createJWT(
      unsignedCommitment,
      (this.signingKey as IPrivateEcKey).d
    );
    const timestamp = Math.floor(new Date().getTime() / 1000);
    const expiry = timestamp + defaultTtlSpan;
    return {
      commitment: signedCommitment,
      exp: expiry,
      iat: timestamp
    };
  }
}

/**
 * Open a serialised ClaimSetKeys JWE
 *
 * @param encryptedClaimSet the claimSetKeys JWE to open
 * @param objectKey the key in which to open the JWE
 */
export const openClaimSet = async (
  encryptedClaimSet: string,
  objectKey: CryptoKey,
  signingKey: IPublicEcKey,
): Promise<ClaimSetKeys> => {
  // Decrypt ClaimSet
  const cryptographer = new Jose.WebCryptographer();
  const decrypter = new Jose.JoseJWE.Decrypter(cryptographer, objectKey);
  const decrypted = await decrypter.decrypt(encryptedClaimSet);
  const decryptedClaimSet = JSON.parse(decrypted);
  if (decryptedClaimSet.commitment) {
    const parts = decryptedClaimSet.commitment.split(".");
    const message = `${parts[0]}.${parts[1]}`;
    if (!ed25519Verifier.verify(signingKey, message, parts[2])) {
      throw new Error("openClaimSet: Could not verify Commitment on claimset");
    }
  }
  const claimSetKeys = new ClaimSetKeys(decryptedClaimSet.sub);
  claimSetKeys.claimSet = decryptedClaimSet.claims;
  claimSetKeys.exp = decryptedClaimSet.exp;
  claimSetKeys.iat = decryptedClaimSet.iat;

  return claimSetKeys;
};

/**
 * Object containing the Metadata for a ClaimSet object
 *
 * @interface IClaimSetMetadata
 */
interface IClaimSetMetadata {
  commitment?: string; // JWS
  iat: number;
  exp: number;
}

/**
 * Object containing all the fields of an unserialised Commitment
 *
 * @interface ICommitent
 */
export interface ICommitent {
  // The signed Commitment string, as a JWS
  commitment: string;
  // The Subject of the Claimset and Attestation
  sub: string;
  // Issued At timestamp
  iss: string;
  // The role of the issuer of this Commitment (Subject, or Attestor)
  role: string;
  // The Public Key to be used to verify this Commitment
  committerPublicKey: IPublicEcKey;
}

export interface IClaimKeys {
  [index: number]: CryptoKey;
}
