/**
 * Created: 2018-12-6 Mischa A. MacLeod <mischa@mysinglesource.io>
 *
 * (c) 2018 by SingleSource Limited, Auckland, New Zealand
 *     http://mysinglesource.io/
 *     Apache 2.0 Licence.
 *
 * This work is licensed under the Apache 2.0 open source licence.
 * Terms and conditions apply.
 *
 * You should have received a copy of the license along with this program.
 */

/**
 * An Extension of the Error object to describe an error relating to HTTP
 */
export class HttpError extends Error {
  public status: number;
  public statusText: string;

  /**
   * Creates an instance of HttpError.
   * @param {string} message the error message to display
   * @param {number} status the http status code eg. 404
   * @param {string} statusText the text of the http error eg. File Not Found
   * @memberof HttpError
   */
  constructor(message: string, status: number, statusText: string) {
    // There is currently a bug with istanbul that treats super calls as a
    // branch, and breaks coverage tests.
    // https://github.com/gotwarlost/istanbul/issues/690
    super(message) /* istanbul ignore next */;
    this.status = status;
    this.statusText = statusText;
  }
}
