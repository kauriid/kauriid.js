const path = require('path');
const DtsBundleWebpack = require('dts-bundle-webpack')

const dtsOptions = {
    name: "kauriidjs",
    main: "dist/src/index.d.ts",
    out: "../kauriid.d.ts"
}

module.exports = {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  plugins: [
    new DtsBundleWebpack(dtsOptions)
  ],
  output: {
    filename: 'kauriid.js',
    path: path.resolve(__dirname, 'dist'),
    library: "kauriId",
    libraryTarget: "umd"
  },
  watchOptions: {
    ignored: /node_modules/
  },
  devtool:'source-map'
};
