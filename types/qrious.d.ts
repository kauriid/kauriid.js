export = qrious;
declare class qrious {
  static class_: string;
  static extend(name: any, constructor: any, prototype: any, statics: any): any;
  static use(service: any): void;
  constructor(options: any);
  get(): any;
  set(options: any): void;
  toDataURL(mime: any): any;
  update(): void;
}